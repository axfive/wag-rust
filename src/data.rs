use crate::ffi;

/// Save the given block of data, overwriting the existing data.
pub fn save(data: &[u8]) {
    unsafe {
        ffi::qwac_data_save(data.as_ptr(), data.len() as _);
    }
}

pub fn size() -> usize {
    unsafe { ffi::qwac_data_size() as usize }
}

/// Load the given data into given buffer.
///
/// Returns a slice of the buffer based on the used length.
pub fn load(data: &mut [u8]) -> &mut [u8] {
    let length = unsafe { ffi::qwac_data_load(data.as_mut_ptr(), data.len() as _) as usize };

    &mut data[0..length]
}

#[cfg(feature = "std")]
pub fn load_vec() -> Vec<u8> {
    let mut output = Vec::new();
    let size = size();
    if size > 0 {
        output.resize(size, 0);
        load(&mut output);
    }
    output
}
