pub use crate::ffi::GamepadButton;

#[derive(Clone, Copy, Hash, Debug, Eq, PartialEq, Ord, PartialOrd)]
pub enum ButtonState {
    Pressed,
    Released,
}

#[derive(Clone, Copy, Hash, Debug, Eq, PartialEq, Ord, PartialOrd)]
#[non_exhaustive]
pub enum Event {
    GamepadButton {
        button: GamepadButton,
        state: ButtonState,
        player: u8,
    },
}

#[derive(Clone, Copy, Hash, Debug, Eq, PartialEq, Ord, PartialOrd, Default)]
struct PressedButtons(u64);

/// Get the bit for the given button and player.
fn button_bit(player: u8, button: GamepadButton) -> u64 {
    1u64 << player as u64 * 12 + button as u64
}

impl PressedButtons {
    fn press(&mut self, player: u8, button: GamepadButton) {
        self.0 |= button_bit(player, button);
    }

    fn release(&mut self, player: u8, button: GamepadButton) {
        self.0 &= !button_bit(player, button);
    }

    fn pressed(&self, player: u8, button: GamepadButton) -> bool {
        (self.0 & button_bit(player, button)) != 0
    }
}

/// Tracks button state.
///
/// Call press and release as appropriate, and call end_frame at the end of the current frame.
#[derive(Clone, Copy, Hash, Debug, Eq, PartialEq, Ord, PartialOrd, Default)]
pub struct ButtonTracker {
    previous_frame: PressedButtons,
    current_frame: PressedButtons,
}

impl ButtonTracker {
    pub fn new() -> Self {
        Default::default()
    }

    pub fn press(&mut self, player: u8, button: GamepadButton) {
        self.current_frame.press(player, button);
    }

    pub fn release(&mut self, player: u8, button: GamepadButton) {
        self.current_frame.release(player, button);
    }

    pub fn end_frame(&mut self) {
        self.previous_frame = self.current_frame;
    }

    /** Get the state of the previous and current frame, in that order.
     */
    pub fn state(&self, player: u8, button: GamepadButton) -> (bool, bool) {
        (
            self.previous_frame.pressed(player, button),
            self.current_frame.pressed(player, button),
        )
    }

    pub fn pressed(&self, player: u8, button: GamepadButton) -> bool {
        self.current_frame.pressed(player, button)
    }

    pub fn just_pressed(&self, player: u8, button: GamepadButton) -> bool {
        let (previous, current) = self.state(player, button);
        !previous && current
    }

    pub fn just_released(&self, player: u8, button: GamepadButton) -> bool {
        let (previous, current) = self.state(player, button);
        previous && !current
    }

    pub fn held(&self, player: u8, button: GamepadButton) -> bool {
        let (previous, current) = self.state(player, button);
        previous && current
    }
}
