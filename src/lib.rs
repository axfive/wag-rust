//! This is a simple crate that makes making qwac games easier.

#![cfg_attr(not(feature = "std"), no_std)]

mod ffi;

pub mod audio;
pub mod canvas;
pub mod color;
pub mod data;
pub mod event;
pub mod texture;

pub use canvas::Canvas;
pub use canvas::DrawDestination;
pub use canvas::Screen;
pub use color::Rgba;
pub use event::Event;
pub use texture::Texture;

/// Just represents the unit for euclidean transforms.
#[derive(Clone, Copy, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Pixels;

#[cfg(feature = "f32-canvas")]
pub type GridUnit = f32;

#[cfg(feature = "f64-canvas")]
pub type GridUnit = f64;

#[cfg(not(any(feature = "f32-canvas", feature = "f64-canvas")))]
pub type GridUnit = i32;

pub type Vector = euclid::Vector2D<GridUnit, Pixels>;
pub type Rect = euclid::Rect<GridUnit, Pixels>;
pub type Size = euclid::Size2D<GridUnit, Pixels>;
pub type Point = euclid::Point2D<GridUnit, Pixels>;

pub trait Game {
    /// Create a new Game of this type, starting at the given time in seconds.
    fn new(timestamp: f64) -> Self;

    /// Handle an input event.
    fn event(&mut self, event: event::Event);

    /// Do processing and rendering.  delta tells you the time in seconds between this frame and
    /// the last one.
    fn update(&mut self, frame_milliseconds: u32);
}

#[macro_export]
macro_rules! qwac_game {
    ($game_type:ty) => {
        #[cfg(not(feature = "std"))]
        static mut QWAC_RUST_GAME: Option<$game_type> = None;

        #[no_mangle]
        pub extern "C" fn qwac_game_create(timestamp: f64) -> *mut $game_type {
            let game = <$game_type as $crate::Game>::new(timestamp);

            #[cfg(not(feature = "std"))]
            {
                unsafe {
                    QWAC_RUST_GAME = None;
                    QWAC_RUST_GAME = Some(game);
                }
                core::ptr::null_mut()
            }

            #[cfg(feature = "std")]
            Box::new(game).into_raw()
        }

        #[no_mangle]
        pub extern "C" fn qwac_game_event_gamepad(
            _game: *mut $game_type,
            player: u8,
            button: $crate::event::GamepadButton,
            pressed: u8,
        ) {
            #[cfg(not(feature = "std"))]
            let game = unsafe { QWAC_RUST_GAME.as_mut().unwrap() };

            #[cfg(feature = "std")]
            let game = unsafe { &mut *_game };

            let state = if pressed == 0 {
                $crate::event::ButtonState::Released
            } else {
                $crate::event::ButtonState::Pressed
            };

            $crate::Game::event(
                game,
                $crate::event::Event::GamepadButton {
                    button,
                    player,
                    state,
                },
            );
        }

        #[no_mangle]
        pub extern "C" fn qwac_game_update(_game: *mut $game_type, frame_milliseconds: u32) {
            #[cfg(not(feature = "std"))]
            let game = unsafe { QWAC_RUST_GAME.as_mut().unwrap() };

            #[cfg(feature = "std")]
            let game = unsafe { &mut *_game };

            $crate::Game::update(game, frame_milliseconds);
        }

        #[no_mangle]
        pub extern "C" fn qwac_game_destroy(_game: *mut $game_type) {
            #[cfg(not(feature = "std"))]
            unsafe {
                QWAC_RUST_GAME = None;
            }

            #[cfg(feature = "std")]
            unsafe {
                Box::from_raw(game);
            }
        }
    };
}

pub fn log(string: &str) {
    let pointer = string.as_ptr();
    let len = string.len() as u32;
    unsafe {
        ffi::qwac_log(pointer, len);
    }
}
